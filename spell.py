#!/usr/bin/env python3

# SPDX-FileCopyrightText: None
# SPDX-License-Identifier: CC0-1.0

import json
import sys
import re

warnings = []

rex = "./(.*):(.*): (.*)"

for line in map(str.rstrip, sys.stdin):
    match = re.match(rex, line)

    if not match:
        print("no")
        continue

    file_name = match.group(1)
    line_number = match.group(2)
    issue = match.group(3)

    warning = {
        "type": "issue",
        "severity": "minor",
        "check_name": "codespell",
        "description": issue,
        "categories": ["Style"],
        "location": {
            "path": file_name,
            "positions": {"begin": {"line": line_number}},
        }
    }

    warnings.append(warning)

f = open("report.json", "w")
f.write(json.dumps(warnings))
f.close()
